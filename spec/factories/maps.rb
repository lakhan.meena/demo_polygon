FactoryBot.define do
  factory :map do
     area { JSON.parse("{\"type\":\"Polygon\",\"coordinates\":[[[138.164063,-31.090574],[139.658203,-30.486551],[138.999023,-32.212801],[138.164063,-31.090574]]]}")}
  end
end
