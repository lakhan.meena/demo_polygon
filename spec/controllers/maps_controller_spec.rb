require 'rails_helper'
require 'json'

RSpec.describe MapsController, type: :controller do

  ## For Fetch Polygons
  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  ## For Create Polygon
  describe "POST #create" do
    context "with valid params" do
      it 'should save the polygon data' do
        post :create, params: {area: "{\"type\":\"Polygon\",\"coordinates\":[[[138.16411,-31.090111],[139.65111,-30.4811],[138.991112,-32.21131],[138.1673242,-31.09435]]]}"}
        expect(response).to redirect_to maps_path
        #expect(FactoryBot.build(:map)).to be_valid
      end
    end
  end

end
