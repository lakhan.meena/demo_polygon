class MapsController < ApplicationController

	def index
		@polygons = Map.pluck(:area).compact.map(&:to_json)
	end

	def create
		map = Map.new(area: JSON.parse(params[:area]))
    if map.save
      respond_to do |format|
        format.js {render inline: "location.reload();" }
        format.html {redirect_to maps_path}
      end
    end
	end
end

