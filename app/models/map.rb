class Map
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Geospatial

  field :area, type: Polygon

end
